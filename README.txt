This module aims to fill in some gaps in the Modr8 module's permissions.

In the Modr8 module, the teasers for content placed into moderation aren't
visible to those without the "moderate content" permission. However, a user who
has the direct URL to content is able to view it. This module controls access
permissions for that content URL.

This module adds the permissions "view any moderated content" and "view own
moderated content".

"view any moderated content" allows you to give permission to roles who do not
have moderate permission, such as an editor. Users with this permission can view
 and edit the moderated content before it's approved by a moderator.
(Note: In Drupal 6, users with the permission "view any moderated content"
WITHOUT the "moderate content" permission can only view the direct link. They
aren't able to view the teasers. This restriction is added by the Modr8 module
and can't be overridden from within this module in Drupal 6.)

"view own moderated content" allows you to add/remove permission for users to
view their own moderated content. After submission, users who don't have
permission to view their own moderated content are redirected to the homepage.
